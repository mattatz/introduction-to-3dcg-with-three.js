/*
 * main.js
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    scene.add(camera);

    var geometry = new THREE.BoxGeometry(80, 80, 80);

    var mesh = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({ color: 0xff0000 }));
    scene.add(mesh);
    mesh.position.set(0, 0, - 350);

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 1, 1);
    var directionalLight2 = new THREE.DirectionalLight(0xffffff);
    directionalLight2.position.set(0, 0, 1);
    scene.add(directionalLight);
    scene.add(directionalLight2);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var clock = new THREE.Clock();

    (function loop() {
        // 回転する軸ベクトル : (0, 1, 0)
        // 回転する角度 clock.elapsedTime
        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), clock.elapsedTime);
        mesh.rotation.setFromQuaternion(q);

        clock.getDelta(); // update clock

        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

