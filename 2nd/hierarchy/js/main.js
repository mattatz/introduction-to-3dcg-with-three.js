/*
 * main.js
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    scene.add(camera);

    var geometry = new THREE.BoxGeometry(30, 30, 30);

    var mesh1 = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({ color: 0xff0000 }));
    scene.add(mesh1);
    mesh1.position.set(0, 0, - 350);

    var mesh2 = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({ color: 0x0000ff }));
    mesh1.add(mesh2); // オブジェクトの階層構造をセット

    mesh2.position.set(100, 100, 0); // 子オブジェクトのローカル座標をセット

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(100, 200, 150);
    scene.add(directionalLight);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var clock = new THREE.Clock();

    (function loop() {

        // 親オブジェクトの座標を動かすと、
        // 子オブジェクトの座標も動く
        mesh1.position.x = Math.cos(clock.elapsedTime * 0.5) * 100;
        mesh1.position.y = Math.sin(clock.elapsedTime) * 100;

        // スケールも影響を受ける（回転も）
        var s = (Math.sin(clock.elapsedTime) + 1.0) * 0.5;
        mesh1.scale.set(s, s, s);

        clock.getDelta(); // update clock

        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

