/*
 * main.js
 *
 * http://wgld.org/d/webgl/w049.html
 * http://qiita.com/edo_m18/items/74ec5832a07f11128f0e
 * http://asura.iaigiri.com/OpenGL/gl45.html
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;

    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    camera.position.z = 100;
    scene.add(camera);

    // 仮想的な光源を表すオブジェクト
    var light = new THREE.Object3D();
    light.position.set(0, 0, 100);

    // テクスチャ座標系から射影座標系への変換行列
    var tMatrix = new THREE.Matrix4();

    // 10th/射影テクスチャマッピングの例と違い、y座標の反転処理が入っていないことに注意
    tMatrix.set(
        0.5,    0, 0, 0.5,
        0,    0.5, 0, 0.5,
        0,      0, 1,   0,
        0,      0, 0,   1
    );

    var lightPov = 60;
    var lightNear = 0.1;
    var lightFar = 1500;

    // 光源のプロジェクション変換行列
    var lpMatrix = new THREE.Matrix4();
    lpMatrix.makePerspective(lightPov, 1, lightNear, lightFar);

    // 光源のビュー変換行列
    var lvMatrix = new THREE.Matrix4();
    lvMatrix.getInverse(light.matrixWorld);

    // 光源の当たる範囲のデバッグ用オブジェクト
    var projCam = new THREE.PerspectiveCamera(lightPov, 1, lightNear, lightFar);
    light.add(projCam);
    
    var camHelper = new THREE.CameraHelper(projCam);
    
    var shadowMat = new THREE.RawShaderMaterial({
        vertexShader: document.getElementById('vertexShaderShadowMapping').textContent,
        fragmentShader: document.getElementById('fragmentShaderShadowMapping').textContent,
        uniforms : {
            "lpMatrix" : { type : "m4", value : lpMatrix },
            "lvMatrix" : { type : "m4", value : lvMatrix },
            "lightNear" : { type : "f", value : lightNear },
            "lightFar" : { type : "f", value : lightFar }
        }
    });

    var shadowMap = new THREE.WebGLRenderTarget(
        1024, // width
        1024, // height,
        {
            type : THREE.UnsignedByteType,
            magFilter: THREE.LinearFilter,
            minFilter: THREE.LinearFilter
        }
    );

    var debugTex = THREE.ImageUtils.loadTexture("images/uv_checker.png");
    var renderMat = new THREE.RawShaderMaterial({
        vertexShader: document.getElementById('vertexShaderRendering').textContent,
        fragmentShader: document.getElementById('fragmentShaderRendering').textContent,
        uniforms : {
            "shadowTex"     : { type : "t", value : shadowMap },
            "debugTex"       : { type : "t", value : debugTex },

            "tMatrix" : { type : "m4", value : tMatrix },
            "lpMatrix" : { type : "m4", value : lpMatrix },
            "lvMatrix" : { type : "m4", value : lvMatrix },
            "lightPosition" : { type : "v3", value : light.position },

            "lightNear" : { type : "f", value : lightNear },
            "lightFar" : { type : "f", value : lightFar },
            "debug" : { type : "i", value : 0 }
        }
    });

    var wall = new THREE.Mesh(new THREE.PlaneGeometry(1, 1), null);
    var box = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), null);
    var torus = new THREE.Mesh(new THREE.TorusGeometry(1, 0.2, 32, 32), null);

    scene.add(wall);
    scene.add(box);
    scene.add(torus);

    wall.position.set(0, 0, -1000);
    wall.scale.set(500, 500, 500);

    box.position.set(5, 0, - 80);
    box.scale.set(30, 30, 30);

    torus.position.set(-30, 30, - 120);
    torus.scale.set(30, 30, 30);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var clock = new THREE.Clock();
    var trackballControls = new THREE.TrackballControls(camera);

    var effectController = {
        debug : false
    };

    var gui = new dat.GUI();
	gui.add(effectController, "debug").onChange(function() {
        renderMat.uniforms["debug"].value = effectController.debug ? 1 : 0;
    });

    (function loop() {
        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), clock.elapsedTime);
        torus.rotation.setFromQuaternion(q);

        var q2 = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0.5, 0.5, 0), clock.elapsedTime);
        box.rotation.setFromQuaternion(q2);

        var t = Math.sin(clock.elapsedTime * 0.5);
        wall.position.z = - 800 + t * 600;
        torus.position.x = - 30 + t * 10;

        var delta = clock.getDelta(); // update clock
        trackballControls.update(delta);

        // render to shadow map ---

        light.updateMatrixWorld();
        lvMatrix.getInverse(light.matrixWorld);

        shadowMat.uniforms.lvMatrix.value = lvMatrix;
        torus.material = box.material = wall.material = shadowMat;

        scene.remove(camHelper);

        renderer.render(scene, projCam, shadowMap);

        // --- render to shadow map

        // render to screen ---

        renderMat.uniforms.lvMatrix.value = lvMatrix;
        torus.material = box.material = wall.material = renderMat;

        scene.add(camHelper);

        renderer.render(scene, camera);

        // --- render to screen

        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

