/*
 * main.js
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    scene.add(camera);

    var mat = new THREE.RawShaderMaterial({
        vertexShader: document.getElementById('vertexShader').textContent,
        fragmentShader: document.getElementById('fragmentShader').textContent,
        uniforms : {
            invMatrix : { type : 'm4', value : null }
        }
    });

    var geometry = new THREE.SphereGeometry(1, 8, 8);
    var sphere = new THREE.Mesh(geometry, mat);
    scene.add(sphere);

    sphere.scale.set(30, 30, 30);
    sphere.position.set(0, 0, - 100);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var clock = new THREE.Clock();

    (function loop() {
        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), clock.elapsedTime * 0.2);
        sphere.rotation.setFromQuaternion(q);

        var inv = (new THREE.Matrix4()).getInverse(sphere.matrix);
        mat.uniforms.invMatrix.value = inv;

        clock.getDelta(); // update clock

        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

