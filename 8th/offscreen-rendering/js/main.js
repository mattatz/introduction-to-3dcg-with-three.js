/*
 * main.js
 * */

function main(e) {

    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();
    var subScene = new THREE.Scene();

    var fov = 60;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, width / height, near, far);
    var subCamera = new THREE.PerspectiveCamera(fov, 1.0, near, far);
    subCamera.position.z = camera.position.z = 5;

    var light = new THREE.DirectionalLight();
    light.position.set(1, 0.5, 1);
    scene.add(light);

    var subLight = new THREE.DirectionalLight();
    subLight.position.set(0.1, 0, 0.5);
    subScene.add(subLight);

    var torus = new THREE.Mesh(
        new THREE.TorusGeometry( 1, 0.5, 16, 100 ),
        new THREE.MeshLambertMaterial({
            color : new THREE.Color(0x0000ff)
        })
    );
    subScene.add(torus);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var renderTarget = new THREE.WebGLRenderTarget(
        512, // width
        512, // height,
        {}
    );

    var planeGeometry = new THREE.PlaneBufferGeometry(2, 2, 1, 1);
    var planeMat = new THREE.MeshBasicMaterial({
        color: 0xffffff,
        map: renderTarget
    });
    
    var center = new THREE.Mesh(planeGeometry, planeMat);
    center.rotation.set(0, 0, 0);
    center.position.set(0, 0, 0);
    
    var left = new THREE.Mesh(planeGeometry, planeMat);
    left.rotation.set(0, Math.PI * 0.4, 0);
    left.position.set(-1.5, 0, 0.5);

    var right = new THREE.Mesh(planeGeometry, planeMat);
    right.rotation.set(0, -Math.PI * 0.4, 0);
    right.position.set(1.5, 0, 0.5);

    var up = new THREE.Mesh(planeGeometry, planeMat);
    up.rotation.set(Math.PI * 0.4, 0, 0);
    up.position.set(0, 1.5, 0.5);

    var down = new THREE.Mesh(planeGeometry, planeMat);
    down.rotation.set(- Math.PI * 0.4, 0, 0);
    down.position.set(0, -1.5, 0.5);

    scene.add(center);
    scene.add(left);
    scene.add(right);
    scene.add(up);
    scene.add(down);

    var clock = new THREE.Clock();

    (function loop() {
        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), clock.elapsedTime * 0.4);
        torus.rotation.setFromQuaternion(q);

        clock.getDelta(); // update clock

        // render to framebuffer
        renderer.setClearColor(0xffffff, 1);
        renderer.render(subScene, subCamera, renderTarget);

        // render to screen
        renderer.setClearColor(0x000000, 1);
        renderer.render(scene, camera);

        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

