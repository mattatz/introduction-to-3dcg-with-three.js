/*
 * main.js
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    scene.add(camera);

    var geometry = new THREE.Geometry();

    // ポリゴンメッシュを構成する頂点の位置を指定する
    geometry.vertices = [
        new THREE.Vector3( 1,  1, 0),
        new THREE.Vector3( 1, -1, 0),
        new THREE.Vector3(-1, -1, 0),
        new THREE.Vector3(-1,  1, 0),
    ];

    // それぞれの面を構成する3つの頂点のindexを指定する
    geometry.faces = [
        new THREE.Face3(0, 2, 1),
        new THREE.Face3(0, 3, 2),
    ];

    var quad = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: 0xff0000 }));
    scene.add(quad);
    quad.position.set(0, 0, - 5);

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 1, 1);
    var directionalLight2 = new THREE.DirectionalLight(0xffffff);
    directionalLight2.position.set(0, 0, 1);
    scene.add(directionalLight);
    scene.add(directionalLight2);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var clock = new THREE.Clock();

    (function loop() {
        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), clock.elapsedTime);
        quad.rotation.setFromQuaternion(q);

        clock.getDelta(); // update clock

        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

