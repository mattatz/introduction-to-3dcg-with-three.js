/*
 * main.js
 * */

function main(e) {

    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    scene.add(camera);

    var geometry = new THREE.BoxGeometry(100, 100, 100);
    var material = new THREE.MeshBasicMaterial();
    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.set(0, 0, - 350);
    scene.add(mesh);

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(100, 200, 150);
    scene.add(directionalLight);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    (function loop() {
        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

