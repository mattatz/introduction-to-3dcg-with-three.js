/*
 * main.js
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    scene.add(camera);

    var geometry = new THREE.SphereGeometry(5, 32, 32);

    var meshes = [
        new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: 0xff0000 })),
        new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({ color: 0xff0000 })),
        new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({ color: 0xff0000 })),
        new THREE.Mesh(geometry, new THREE.MeshNormalMaterial({ color: 0xff0000 })),
        new THREE.Mesh(geometry, new THREE.MeshDepthMaterial({ color: 0xff0000 }))
    ];

    var n = meshes.length;
    meshes.forEach(function(mesh, i) {
        var r = i / (n - 1);
        mesh.position.set((r - 0.5) * 80, 0, -60);
        scene.add(mesh);
    });

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 1, 1);
    var directionalLight2 = new THREE.DirectionalLight(0xffffff);
    directionalLight2.position.set(0, 0, 1);
    scene.add(directionalLight);
    scene.add(directionalLight2);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    (function loop() {
        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();
}

window.addEventListener('DOMContentLoaded', main, false);

