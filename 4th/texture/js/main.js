/*
 * main.js
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    scene.add(camera);

    var geometry = new THREE.Geometry();

    geometry.vertices = [
        new THREE.Vector3( 1,  1, 0),
        new THREE.Vector3( 1, -1, 0),
        new THREE.Vector3(-1, -1, 0),
        new THREE.Vector3(-1,  1, 0),
    ];

    geometry.faces = [
        new THREE.Face3(0, 2, 1),
        new THREE.Face3(0, 3, 2),
    ];

    /*
     * uv座標とテクスチャ空間との関係
     * (0,1)--------(1,1)
     *    |         |
     *    |         |
     *    |         |
     *    |         |
     * (0,0)--------(1,0)
     * */

    // (0, 2, 1)の面のuv座標
    geometry.faceVertexUvs[0].push(
        [
            new THREE.Vector2(1, 1),
            new THREE.Vector2(0, 0),
            new THREE.Vector2(1, 0)
        ]
    );

    // (0, 3, 2)の面のuv座標
    geometry.faceVertexUvs[0].push(
        [
            new THREE.Vector2(1, 1),
            new THREE.Vector2(0, 1),
            new THREE.Vector2(0, 0)
        ]
    );

    var mesh = new THREE.Mesh(
        geometry,
        new THREE.MeshBasicMaterial({ 
            map : THREE.ImageUtils.loadTexture("textures/teamlab.png")
        })
    );

    mesh.position.set(0, 0, -2);
    scene.add(mesh);

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 1, 1);
    var directionalLight2 = new THREE.DirectionalLight(0xffffff);
    directionalLight2.position.set(0, 0, 1);
    scene.add(directionalLight);
    scene.add(directionalLight2);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    (function loop() {
        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();
}

window.addEventListener('DOMContentLoaded', main, false);

