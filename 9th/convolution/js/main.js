/*
 * main.js
 * */

function main(e) {

    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var near = 1;
    var far = 1000;
    var camera = new THREE.PerspectiveCamera(fov, width / height, near, far);
    camera.position.z = 5;

    var box = new THREE.Mesh(
        new THREE.BoxGeometry(2, 2, 2),
        new THREE.MeshBasicMaterial({
            color : 0xffffff,
            map : THREE.ImageUtils.loadTexture("box.jpg")
        })
    );
    scene.add(box);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var renderTarget = new THREE.WebGLRenderTarget(
        2048, // width
        2048, // height,
        {
            type : THREE.FloatType,
            magFilter: THREE.LinearFilter,
            minFilter: THREE.LinearFilter,
            wrapS: THREE.RepeatWrapping,
            wrapT: THREE.RepeatWrapping
        }
    );

    var clock = new THREE.Clock();

	var peCamera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1);
    var peScene = new THREE.Scene();

	var peMesh = new THREE.Mesh(
        new THREE.PlaneBufferGeometry(2, 2),
        new THREE.RawShaderMaterial({
            vertexShader    : document.getElementById("vertexShader").textContent,
            fragmentShader  : document.getElementById("fragmentShader").textContent,
            uniforms : {
                screenTex : { type : "t", value : null },
                px        : { type : "v2", value : new THREE.Vector2(1.0 / renderTarget.width, 1.0 / renderTarget.height) },
                kernel    : { type : "fv1", value : [] }
            }
        })
    );
	peScene.add(peMesh);

    var kernels = [
        {
            name : "identity",
            kernel : [
                0.0,  0.0,  0.0,
                0.0,  1.0,  0.0,
                0.0,  0.0,  0.0
            ]
        },
        {
            name : "gaussian blur",
            kernel : [
                1/16, 2/16, 1/16,
                2/16, 4/16, 2/16,
                1/16, 2/16, 1/16
            ]
        },
        {
            name : "sharpen",
            kernel : [
                 0.0, -1.0,  0.0,
                -1.0,  5.0, -1.0,
                 0.0, -1.0,  0.0
            ]
        },
        {
            name : "edge detection",
            kernel : [
                -1.0, -1.0, -1.0,
                -1.0,  8.0, -1.0,
                -1.0, -1.0, -1.0
            ]
        },
        {
            name : "x gradient",
            kernel : [
                -1.0,  0.0,  1.0,
                -2.0,  0.0,  2.0,
                -1.0,  0.0,  1.0
            ]
        },
        {
            name : "y gradient",
            kernel : [
                -1.0, -2.0, -1.0,
                 0.0,  0.0,  0.0,
                 1.0,  2.0,  1.0
            ]
        }
    ];

    // identity
    peMesh.material.uniforms.kernel.value = kernels[0].kernel;

    var controller = {
        kernel : 0
    };

    var gui = new dat.GUI();
    var names = kernels.map(function(k) {
        return k.name;
    });
	gui.add(controller, "kernel", names).onChange(
        function(name) {
            var idx = names.indexOf(name);
            peMesh.material.uniforms.kernel.value = kernels[idx].kernel;    
        }
    );
	gui.close();

    (function loop() {
        var t = clock.elapsedTime * 0.5;
        box.rotation.set(t, t, 0.0);

        clock.getDelta(); // update clock

        // render to screen
        renderer.setClearColor(0xffffff, 1);
        renderer.render(scene, camera, renderTarget);

        // set color buffer to post effect material.
        peMesh.material.uniforms.screenTex.value = renderTarget;

        renderer.setClearColor(0xffffff, 1);
        renderer.render(peScene, peCamera);

        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

