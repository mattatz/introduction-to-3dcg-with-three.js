/*
 * main.js
 * */

function main(e) {

    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, width / height, near, far);
    camera.position.z = 5;

    var light = new THREE.DirectionalLight();
    light.position.set(1, 0.5, 1);
    scene.add(light);

    var torus = new THREE.Mesh(
        new THREE.TorusGeometry( 1, 0.5, 16, 100 ),
        new THREE.MeshPhongMaterial({
            color : 0x0000ff
        })
    );
    scene.add(torus);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var renderTarget = new THREE.WebGLRenderTarget(
        512, // width
        512, // height,
        {
            type : THREE.FloatType,
            magFilter: THREE.LinearFilter,
            minFilter: THREE.LinearFilter,
            wrapS: THREE.RepeatWrapping,
            wrapT: THREE.RepeatWrapping
        }

    );

    var clock = new THREE.Clock();

	var peCamera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1);
    var peScene = new THREE.Scene();

	var peMesh = new THREE.Mesh(
        new THREE.PlaneBufferGeometry(2, 2),
        new THREE.RawShaderMaterial({
            vertexShader    : document.getElementById("vertexShader").textContent,
            fragmentShader  : document.getElementById("fragmentShader").textContent,
            uniforms : {
                screenTex : { type : "t", value : null },
                px        : { type : "v2", value : new THREE.Vector2(1.0 / renderTarget.width, 1.0 / renderTarget.height) },
                ratio     : { type : "f", value : width / height },
                size      : { type : "f", value : 15.0 }
            }
        })
    );
	peScene.add(peMesh);

    var effectController = {
        size : 15.0,
    };

    var matChanger = function() {
        peMesh.material.uniforms["size"].value = effectController.size;
    };

    var gui = new dat.GUI();
	gui.add(effectController, "size", 1.0, 30.0, 0.5 ).onChange(matChanger);
	gui.close();

    (function loop() {
        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), clock.elapsedTime * 0.5);
        torus.rotation.setFromQuaternion(q);

        clock.getDelta(); // update clock

        // render to screen
        renderer.setClearColor(0x000000, 1);
        renderer.render(scene, camera, renderTarget);

        // set color buffer to post effect material.
        peMesh.material.uniforms.screenTex.value = renderTarget;

        renderer.setClearColor(0x000000, 1);
        renderer.render(peScene, peCamera);

        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

