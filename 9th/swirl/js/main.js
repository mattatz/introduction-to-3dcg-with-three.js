/*
 * main.js
 * */

function main(e) {

    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var near = 1;
    var far = 10000;
    var camera = new THREE.PerspectiveCamera(fov, width / height, near, far);
    camera.position.z = 5;

    var light = new THREE.DirectionalLight();
    light.position.set(1, 0.5, 1);
    scene.add(light);

    var plane = new THREE.Mesh(
        new THREE.PlaneBufferGeometry(15, 15, 64, 64),
        new THREE.MeshPhongMaterial({
            color : 0xffffff,
            wireframe : true
        })
    );
    scene.add(plane);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var renderTarget = new THREE.WebGLRenderTarget(
        1024, // width
        1024, // height,
        {
            type : THREE.FloatType,
            magFilter: THREE.LinearFilter,
            minFilter: THREE.LinearFilter,
            wrapS: THREE.RepeatWrapping,
            wrapT: THREE.RepeatWrapping
        }
    );

    var clock = new THREE.Clock();

	var peCamera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1);
    var peScene = new THREE.Scene();

	var peMesh = new THREE.Mesh(
        new THREE.PlaneBufferGeometry(2, 2),
        new THREE.RawShaderMaterial({
            vertexShader    : document.getElementById("vertexShader").textContent,
            fragmentShader  : document.getElementById("fragmentShader").textContent,
            uniforms : {
                screenTex : { type : "t", value : null },
                power    : { type : "f", value : 1.0 },
                radius   : { type : "f", value : 0.5 }
            }
        })
    );
	peScene.add(peMesh);

    var effectController = {
        power  : 1.0,
        radius : 0.5
    };

    var matChanger = function() {
        peMesh.material.uniforms["power"].value = effectController.power;
        peMesh.material.uniforms["radius"].value = effectController.radius;
    };

    var gui = new dat.GUI();
	gui.add(effectController, "power", 0.0, 10.0, 0.1 ).onChange(matChanger);
	gui.add(effectController, "radius", 0.0, 1.0, 0.1 ).onChange(matChanger);
	gui.close();

    (function loop() {

        clock.getDelta(); // update clock

        // render to screen
        renderer.setClearColor(0x000000, 1);
        renderer.render(scene, camera, renderTarget);

        // set color buffer to post effect material.
        peMesh.material.uniforms.screenTex.value = renderTarget;

        renderer.setClearColor(0x000000, 1);
        renderer.render(peScene, peCamera);

        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

