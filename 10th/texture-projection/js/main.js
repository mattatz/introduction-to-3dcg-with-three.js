/*
 * main.js
 *
 * http://wgld.org/d/webgl/w049.html
 * http://qiita.com/edo_m18/items/74ec5832a07f11128f0e
 * http://asura.iaigiri.com/OpenGL/gl45.html
 * */

function main(e) {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var scene = new THREE.Scene();

    var fov = 60;
    var aspect = width / height;
    var near = 1;
    var far = 10000;

    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    camera.position.z = 100;
    scene.add(camera);

    // プロジェクションを行う視点オブジェクト
    var proj = new THREE.Object3D();
    proj.position.set(0, 0, 50);
    proj.lookAt(new THREE.Vector3());
    proj.updateMatrixWorld();

    scene.add(proj);

    // テクスチャ座標系から射影座標系への変換行列
    var tMatrix = new THREE.Matrix4();
    tMatrix.set(
        0.5,    0, 0, 0.5,
        0,   -0.5, 0, 0.5,
        0,      0, 1,   0,
        0,      0, 0,   1
    );

    var tPov = 60;
    var tNear = 0.1;
    var tFar = 10000;

    // プロジェクション変換行列
    var tpMatrix = new THREE.Matrix4();
    tpMatrix.makePerspective(tPov, 1, tNear, tFar);

    // ビュー変換行列
    var tvMatrix = new THREE.Matrix4();
    tvMatrix.getInverse(proj.matrixWorld);
 
    // テクスチャ変換行列 * プロジェクション変換行列 * ビュー変換行列
    var texProjMatrix = new THREE.Matrix4();
    texProjMatrix.multiplyMatrices(tMatrix, tpMatrix);
    texProjMatrix.multiplyMatrices(texProjMatrix, tvMatrix);

    // プロジェクションする視点のデバッグオブジェクト
    var projCam = new THREE.PerspectiveCamera(tPov, 1, tNear, tFar);
    projCam.lookAt(new THREE.Vector3(0, 0, 1));
    proj.add(projCam);
    var camHelper = new THREE.CameraHelper(projCam);
    scene.add(camHelper);
   
    var tex = THREE.ImageUtils.loadTexture("images/uv_checker.png");
    // tex.wrapS = tex.wrapT = THREE.RepeatWrapping;
    var mat = new THREE.RawShaderMaterial({
        vertexShader: document.getElementById('vertexShader').textContent,
        fragmentShader: document.getElementById('fragmentShader').textContent,
        uniforms : {
            "projTex"       : { type : "t", value : tex },
            "texProjMatrix" : { type : "m4", value : texProjMatrix }
        }
    });

    var wall = new THREE.Mesh(new THREE.PlaneGeometry(1, 1), mat);
    var box = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), mat);
    var torus = new THREE.Mesh(new THREE.TorusGeometry(1, 0.2, 32, 32), mat);

    scene.add(wall);
    scene.add(box);
    scene.add(torus);

    wall.position.set(0, 0, -1000);
    wall.scale.set(500, 500, 500);

    box.position.set(10, 0, 10);
    box.scale.set(30, 30, 30);

    torus.position.set(-30, 0, 0);
    torus.scale.set(30, 30, 30);

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    document.body.appendChild(renderer.domElement);

    var clock = new THREE.Clock();
    var trackballControls = new THREE.TrackballControls(camera);

    (function loop() {
        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 0, 1), clock.elapsedTime);

        var q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), clock.elapsedTime);
        torus.rotation.setFromQuaternion(q);

        var t = Math.sin(clock.elapsedTime * 0.5);
        wall.position.z = - 1000 + t * 800;
        torus.position.x = - 30 + t * 10;

        var delta = clock.getDelta(); // update clock
        trackballControls.update(delta);

        camHelper.update();

        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    })();

}

window.addEventListener('DOMContentLoaded', main, false);

